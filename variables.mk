########### HERE BEGINS YOUR SETUP!! ###################

# The Docker image tag to use when buildng
CONTAINER_IMAGE_TAG := registry.gitlab.com/diegoortizmatajira/heimdall-plan-structure
# Driver used by the ORM Code generator
ORM_DRIVER := crdb
# Driver used by the migrations tool
MIGRATIONS_DRIVER := postgres
# Connection string used by the migration tool
CONNECTION_STRING := postgresql://root@localhost:26257/heimdall-plan-structure?sslmode=disable

########### FOLLOWING SETTINGS ARE EXPECTED TO REMAIN STANDARD ###################
########### ONLY MODIFY THEM IF YOU REALLY REQUIRE IT!! ######################### 

CODE_API_FOLDER := ./api
CODE_CMD_FOLDER := ./cmd
CODE_INTERNAL_FOLDER := ./internal
CODE_SCRIPTS_FOLDER := ./scripts
CODE_PROTO_FOLDER := $(CODE_API_FOLDER)/grpc
CODE_DOMAIN_FOLDER := $(CODE_INTERNAL_FOLDER)/.../domain
CODE_ORM_FOLDER := $(CODE_INTERNAL_FOLDER)/infrastructure/orm
CODE_DOCKERFILE := ./build/dockerfile
MIGRATIONS_FOLDER := $(CODE_SCRIPTS_FOLDER)/migrations


#Paths to folders containing code to be reviewed by go vet & go fmt
CODE_FOLDERS := \
	$(CODE_API_FOLDER) \
	$(CODE_CMD_FOLDER) \
	$(CODE_SCRIPTS_FOLDER) \
	$(CODE_INTERNAL_FOLDER) \
	$(CODE_ORM_FOLDER) \

########### HERE ENDS YOUR SETUP!! ###################