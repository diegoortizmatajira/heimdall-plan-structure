//+build wireinject

package wiring

import (
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/services"

	"gitlab.com/diegoortizmatajira/go-common/details/logging"

	"github.com/google/wire"
	repositories "gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/adapters/sql-repositories"
)

var dependencyInjectionSet = wire.NewSet(
	logging.DependencyInjectionSet,
	repositories.DependencyInjectionSet,
	services.NewPlanService,
)

func GetPlanService() services.PlanService {
	wire.Build(dependencyInjectionSet)
	return nil
}
