package domain

import (
	"gitlab.com/diegoortizmatajira/go-common/base"
	"gitlab.com/diegoortizmatajira/go-common/validators"

	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/gofrs/uuid"
)

// TaskContrains provides the constrains for the Location fields
var TaskContrains = struct {
	NameMaxLength        int
	DescriptionMaxLength int
}{
	NameMaxLength:        60,
	DescriptionMaxLength: 400,
}

// Task represents a Task entity
type Task struct {
	base.Entity
	Name        string
	Description string
	PlantID     uuid.UUID
}

// Validate ensures the data in a Task is valid
func (l *Task) Validate() error {
	return validation.ValidateStruct(l,
		validation.Field(&l.Entity, validation.NotNil),
		validation.Field(&l.Name, validation.Required, validation.Length(1, TaskContrains.NameMaxLength)),
		validation.Field(&l.Description, validation.Required, validation.Length(1, TaskContrains.DescriptionMaxLength)),
		validation.Field(&l.PlantID, validators.RequiredUUID),
	)
}

// TaskRepository represents the data layer for the Task entity
type TaskRepository interface {
}
