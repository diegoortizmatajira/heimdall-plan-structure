package domain

import (
	"context"

	"gitlab.com/diegoortizmatajira/go-common/base"

	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/gofrs/uuid"
)

// PlanContrains provides the constrains for the Location fields
var PlanContrains = struct {
	NameMaxLength        int
	DescriptionMaxLength int
}{
	NameMaxLength:        60,
	DescriptionMaxLength: 400,
}

// Plan represents a plan entity
type Plan struct {
	*base.Entity
	Name        string
	Description string
}

// NewPlan creates a new instance of Plan
func NewPlan() *Plan {
	return &Plan{
		Entity: base.NewEntity(),
	}
}

// Validate ensures the data in a Task is valid
func (l *Plan) Validate() error {
	return validation.ValidateStruct(l,
		validation.Field(&l.Entity, validation.NotNil),
		validation.Field(&l.Name, validation.Required, validation.Length(1, PlanContrains.NameMaxLength)),
		validation.Field(&l.Description, validation.Required, validation.Length(1, PlanContrains.DescriptionMaxLength)),
	)
}

// PlanRepository represents the data layer for the Plan entity
type PlanRepository interface {
	Get(context.Context, uuid.UUID) (*Plan, error)
	Insert(context.Context, *Plan) error
	Update(context.Context, *Plan) error
	Delete(context.Context, *Plan) error
}
