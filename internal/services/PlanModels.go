package services

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/domain"
)

// PlanService represents the service layer for the Plan entity
type PlanService interface {
	QueryUserPlans(context.Context, QueryUserPlansRequest) (*QueryPlanResponse, error)
	Save(context.Context, SavePlanRequest) (*SavePlanResponse, error)
	Delete(context.Context, DeletePlanRequest) (*DeletePlanResponse, error)
}

// PlanDTO provides the data from a Plan entity
type PlanDTO struct {
	ID          uuid.UUID
	Name        string
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

// QueryUserPlansRequest provides the required data for a QueryUserPlans operation
type QueryUserPlansRequest struct {
	UserID     uuid.UUID
	PageSize   int
	PageNumber int
}

// QueryPlanResponse provides the returned data for a Query operation
type QueryPlanResponse struct {
	Plans []*PlanDTO
	Count int
}

// SavePlanRequest provides the required data for a Save operation
type SavePlanRequest struct {
	ID          uuid.UUID
	Name        string
	Description string
}

// SavePlanResponse provides the returned data for a Save operation
type SavePlanResponse struct {
	ConfirmedData PlanDTO
}

func newSavePlanResponse(entity *domain.Plan) *SavePlanResponse {
	return &SavePlanResponse{
		ConfirmedData: PlanDTO{
			ID:          entity.ID,
			Name:        entity.Name,
			Description: entity.Description,
			CreatedAt:   entity.CreatedAt,
			UpdatedAt:   entity.UpdatedAt,
		},
	}
}

// DeletePlanRequest provides the required data for a Delete operation
type DeletePlanRequest struct {
}

// DeletePlanResponse provides the returned data for a Delete operation
type DeletePlanResponse struct {
}
