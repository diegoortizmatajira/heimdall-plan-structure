package services

import (
	"context"

	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/domain"

	"github.com/gofrs/uuid"
	"github.com/micro/go-micro/v2/logger"
	"gitlab.com/diegoortizmatajira/go-common/base"
)

type planService struct {
	*base.Component
	uow  base.UnitOfWork
	repo domain.PlanRepository
}

func (s *planService) QueryUserPlans(ctx context.Context, req QueryUserPlansRequest) (*QueryPlanResponse, error) {
	return nil, nil
}

func (s *planService) Save(ctx context.Context, req SavePlanRequest) (*SavePlanResponse, error) {
	s.uow.Begin(ctx)
	defer s.uow.DeferredRollback()
	entity := domain.NewPlan()
	{
		entity.ID = req.ID
		entity.Name = req.Name
		entity.Description = req.Description
	}
	if err := entity.Validate(); err != nil {
		return nil, s.Error("Invalid entity values", err)
	}
	if entity.ID == uuid.Nil {
		if err := entity.Entity.GenerateNewID(); err != nil {
			return nil, s.Error("Error on assigning a new ID to record", err)
		}
		if err := s.repo.Insert(ctx, entity); err != nil {
			return nil, s.Error("Error on saving a new Plan", err)
		}
	} else {
		if err := s.repo.Update(ctx, entity); err != nil {
			return nil, s.Error("Error on updating an existing Plan", err)
		}
	}
	s.uow.Complete()
	return newSavePlanResponse(entity), nil
}

func (s *planService) Delete(ctx context.Context, req DeletePlanRequest) (*DeletePlanResponse, error) {
	return &DeletePlanResponse{}, nil
}

// NewPlanService creates a new instance of PlanServices
func NewPlanService(helper *logger.Helper, uow base.UnitOfWork, repo domain.PlanRepository) PlanService {
	return &planService{
		base.NewServiceComponent(helper, "PlanService"),
		uow,
		repo,
	}
}
