package grpc

import (
	"context"

	"github.com/micro/go-micro/v2/logger"
	"gitlab.com/diegoortizmatajira/go-common/base"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/api/grpc"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/wiring"
)

type planServiceHandler struct {
	*base.Component
	planServiceHandlerConverters
}

func newPlanHandler(helper *logger.Helper) *planServiceHandler {
	return &planServiceHandler{
		base.NewHandlerComponent(helper, "grpc", "PlanHandler"),
		planServiceHandlerConverters{},
	}
}

func (h *planServiceHandler) Save(ctx context.Context, req *grpc.SavePlanRequest, res *grpc.SavePlanResponse) error {
	svc := wiring.GetPlanService()
	request := h.newSaveRequest(req)
	response, err := svc.Save(ctx, request)
	if err != nil {
		err = h.Error("Error on processing Save Plan Request", err)
		h.Logger.WithError(err).Error("Error on handler")
		return err
	}
	h.flushSaveResponse(*response, res)
	return nil
}
