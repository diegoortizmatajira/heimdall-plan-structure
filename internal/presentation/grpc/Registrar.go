package grpc

import (
	"github.com/micro/go-micro/v2/server"
	"gitlab.com/diegoortizmatajira/go-common/details/logging"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/api/grpc"
)

// RegisterHandlers registers the handles of the package
func RegisterHandlers(server server.Server) error {
	logger := logging.GetLogger()
	helper := logging.NewLoggerHelper(logger)
	if err := grpc.RegisterPlanServiceHandler(server, newPlanHandler(helper)); err != nil {
		return err
	}
	return nil
}
