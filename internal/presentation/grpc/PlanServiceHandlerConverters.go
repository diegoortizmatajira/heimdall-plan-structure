package grpc

import (
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/api/grpc"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/services"
)

type planServiceHandlerConverters struct {
}

func (c *planServiceHandlerConverters) newPlantItem(data services.PlanDTO) *grpc.PlanItem {
	protoCreatedAt, _ := ptypes.TimestampProto(data.CreatedAt)
	protoUpdatedAt, _ := ptypes.TimestampProto(data.UpdatedAt)
	return &grpc.PlanItem{
		Id:          data.ID.String(),
		Name:        data.Name,
		Description: data.Description,
		CreatedAt:   protoCreatedAt,
		UpdatedAt:   protoUpdatedAt,
	}
}

func (c *planServiceHandlerConverters) newSaveRequest(r *grpc.SavePlanRequest) services.SavePlanRequest {
	return services.SavePlanRequest{
		ID:          uuid.FromStringOrNil(r.Data.Id),
		Description: r.Data.Description,
		Name:        r.Data.Name,
	}
}

func (c *planServiceHandlerConverters) flushSaveResponse(r services.SavePlanResponse, res *grpc.SavePlanResponse) {
	res.ConfirmedData = c.newPlantItem(r.ConfirmedData)
}
