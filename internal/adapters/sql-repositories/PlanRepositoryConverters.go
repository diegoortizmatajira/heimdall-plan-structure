package repositories

import (
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/domain"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/infrastructure/orm"
)

type planRepositoryConverters struct {
}

func (c *planRepositoryConverters) convertEntityToRecord(src *domain.Plan) (*orm.Plan, error) {
	dest := &orm.Plan{}
	dest.CreatedAt = src.CreatedAt
	dest.Description = src.Description
	dest.ID = src.ID
	dest.Name = src.Name
	dest.UpdatedAt = src.UpdatedAt
	return dest, nil
}

func (c *planRepositoryConverters) updateEntityFromRecord(src *orm.Plan, dest *domain.Plan) error {
	dest.CreatedAt = src.CreatedAt
	dest.Description = src.Description
	dest.ID = src.ID
	dest.Name = src.Name
	dest.UpdatedAt = src.UpdatedAt
	return nil
}

func (c *planRepositoryConverters) convertRecordToEntity(src *orm.Plan) (*domain.Plan, error) {
	dest := domain.NewPlan()
	if err := c.updateEntityFromRecord(src, dest); err != nil {
		return nil, err
	}
	return dest, nil
}
