package repositories

import (
	"gitlab.com/diegoortizmatajira/go-common/base"
	"gitlab.com/diegoortizmatajira/go-common/details/db"

	"github.com/google/wire"
)

// DependencyInjectionSet exposes constructors to be used for Dependency Injection
var DependencyInjectionSet = wire.NewSet(
	db.NewSQLUnitOfWork,
	wire.Bind(new(base.UnitOfWork), new(db.SQLUnitOfWork)),
	GetDB,
	NewPlanRepository,
)
