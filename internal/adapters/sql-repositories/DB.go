package repositories

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq" //Adds driver for postgres
)

var (
	dbConnection *sql.DB
)

//GetDB returns a DB Connection
func GetDB() *sql.DB {
	return dbConnection
}

//UseConnectionString set the DB Connection to the provided connection string
func UseConnectionString(connStr string) error {
	if connStr == "" {
		return errors.New("No database connection string provided")
	}
	var err error
	dbConnection, err = sql.Open("postgres", connStr)
	return err
}
