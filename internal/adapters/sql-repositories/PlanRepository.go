package repositories

import (
	"context"

	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/domain"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/infrastructure/orm"

	"gitlab.com/diegoortizmatajira/go-common/base"
	"gitlab.com/diegoortizmatajira/go-common/details/db"

	"github.com/gofrs/uuid"
	"github.com/micro/go-micro/v2/logger"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

type planRepository struct {
	*base.Component
	planRepositoryConverters
	uow db.SQLUnitOfWork
}

func (r *planRepository) Get(ctx context.Context, id uuid.UUID) (*domain.Plan, error) {
	exec := r.uow.GetExecutor()
	record, err := orm.FindPlan(ctx, exec, id)
	if err != nil {
		return nil, r.Error("Error at obtaining record from database", err)
	}
	return r.convertRecordToEntity(record)
}

func (r *planRepository) Insert(ctx context.Context, plan *domain.Plan) error {
	exec := r.uow.GetExecutor()
	record, err := r.convertEntityToRecord(plan)
	if err != nil {
		return r.Error("Error at converting Entity to Record", err)
	}
	if err := record.Insert(ctx, exec, boil.Infer()); err != nil {
		return r.Error("Error at insert record on database", err)
	}
	return r.updateEntityFromRecord(record, plan)
}

func (r *planRepository) Update(ctx context.Context, plan *domain.Plan) error {
	exec := r.uow.GetExecutor()
	record, err := r.convertEntityToRecord(plan)
	if err != nil {
		return r.Error("Error at converting Entity to Record", err)
	}
	if _, err := record.Update(ctx, exec, boil.Infer()); err != nil {
		return r.Error("Error at update record on database", err)
	}
	if err := record.Reload(ctx, exec); err != nil {
		return r.Error("Error at refreshing Record data", err)
	}
	return r.updateEntityFromRecord(record, plan)
}

func (r *planRepository) Delete(ctx context.Context, plan *domain.Plan) error {
	exec := r.uow.GetExecutor()
	record, err := r.convertEntityToRecord(plan)
	if err != nil {
		return r.Error("Error at converting Entity to Record", err)
	}
	if _, err := record.Delete(ctx, exec); err != nil {
		return r.Error("Error at delete record on database", err)
	}
	plan = nil
	return nil
}

// NewPlanRepository creates a new instance of Plan Repository
func NewPlanRepository(logger *logger.Helper, uow db.SQLUnitOfWork) domain.PlanRepository {
	return &planRepository{
		base.NewRepositoryComponent(logger, "sql", "PlanRepository"),
		planRepositoryConverters{},
		uow,
	}
}
