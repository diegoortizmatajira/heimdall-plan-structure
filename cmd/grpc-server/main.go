package main

import (
	"fmt"
)

// Version gets the current version of the executable
var Version string

func main() {
	fmt.Println(Version)
	Server := &GRPCServer{}
	Server.Run()
}
