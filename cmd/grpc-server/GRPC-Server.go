package main

import (
	"fmt"

	"github.com/micro/cli/v2"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/config"
	"gitlab.com/diegoortizmatajira/go-common/base"
	repositories "gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/adapters/sql-repositories"
	"gitlab.com/diegoortizmatajira/heimdall-plan-structure/internal/presentation/grpc"
)

// GRPCServer provides the server configuration for the application
type GRPCServer struct {
	base.AppServer
}

// InitializeFlags defines which flags are available
func (s *GRPCServer) InitializeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    "log_level",
			Usage:   "Logging detail level (trace, debug, warn, info, error, fatal)",
			EnvVars: []string{"LOG_LEVEL"},
			Value:   "info",
		},
		&cli.StringFlag{
			Name:    "log_format",
			Usage:   "Logging format (text, json)",
			EnvVars: []string{"LOG_FORMAT"},
			Value:   "text",
		},
		&cli.StringFlag{
			Name:    "database_connection",
			Usage:   "Database connection string",
			EnvVars: []string{"DATABASE_CONNECTION"},
			Value:   "postgresql://root@localhost:26257/heimdall-plan-structure?sslmode=disable",
		},
	}
}

// InitializeOptions provides the default options for the server
func (s *GRPCServer) InitializeOptions() []micro.Option {
	return []micro.Option{
		micro.Name("org.heimdall.plan-structure.server"),
		micro.Address("[::]:9080"),
		micro.Version(Version),
	}
}

// InitializeInfrastructure initializes the infrastructure resources
func (s *GRPCServer) InitializeInfrastructure() error {
	// Initializes database
	connStr := config.Get("database", "connection").String("")
	if err := repositories.UseConnectionString(connStr); err != nil {
		return fmt.Errorf("Database initialization error: %w", err)
	}
	return nil
}

// InitializeServiceHandlers registers the service handlers
func (s *GRPCServer) InitializeServiceHandlers(service micro.Service) error {
	if err := grpc.RegisterHandlers(service.Server()); err != nil {
		return err
	}
	return nil
}

// Run executes the application using the settings in this structure
func (s *GRPCServer) Run() {
	s.AppServer.Run(s)
}
