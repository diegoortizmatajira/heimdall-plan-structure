module gitlab.com/diegoortizmatajira/heimdall-plan-structure
go 1.14

require (
	github.com/friendsofgo/errors v0.9.2
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/wire v0.4.0
	github.com/lib/pq v1.7.1
	github.com/micro/cli/v2 v2.1.2
	github.com/micro/go-micro/v2 v2.9.1
	github.com/volatiletech/sqlboiler/v4 v4.2.0
	github.com/volatiletech/strmangle v0.0.1
	gitlab.com/diegoortizmatajira/go-common v0.1.5
)
