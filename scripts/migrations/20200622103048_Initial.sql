-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE "plan" (
    "id" uuid PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "description" varchar(400) NOT NULL,
    "created_at" timestamp WITH TIME ZONE NOT NULL DEFAULT now(),
    "updated_at" timestamp WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE "task" (
    "id" uuid PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "description" varchar(400) NOT NULL,
    "plan_id" uuid NOT NULL REFERENCES "plan" ("id") ON DELETE CASCADE,
    "created_at" timestamp WITH TIME ZONE NOT NULL DEFAULT now(),
    "updated_at" timestamp WITH TIME ZONE NOT NULL DEFAULT now()
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE "task";
DROP TABLE "plan";