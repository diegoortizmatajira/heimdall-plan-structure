include variables.mk

# Make is verbose in Linux. Make it silent.
MAKEFLAGS += --silent

# Used internally.  Users should pass GOOS and/or GOARCH.
ALL_PLATFORMS := linux/amd64 windows/amd64 darwin/amd64
OS := $(if $(GOOS),$(GOOS),$(shell go env GOOS))
ARCH := $(if $(GOARCH),$(GOARCH),$(shell go env GOARCH))
ifeq ($(OS), windows)
	BIN_SUFFIX := .exe
endif
# This version-strategy uses git tags to set the version string
VERSION := $(shell git describe --tags --always --dirty)

# Obtains the CMD folder names to be used when building the project
CMD_NAMES := $(shell find $(CODE_CMD_FOLDER) -mindepth 1 -maxdepth 1 -type d -printf '%f\n')

PKG_LIST := $(shell go list ./... | grep -v /vendor/)

# Directories that we need created to build/test.
BLD_PATH_BIN := bin/$(OS)_$(ARCH)
BLD_PATH_GO_BIN := .go/bin/$(OS)_$(ARCH)
BLD_PATH_GO_CACHE := .go/cache
BUILD_DIRS := $(BLD_PATH_BIN)     \
              $(BLD_PATH_GO_BIN) \
              $(BLD_PATH_GO_CACHE)

.PHONY: all clean build-% all-build build lint test test-race fmt vet scaffold dep tools docker-% gen-% migrate-%

all: build

clean: ## Deletes previous builds
	@rm -rf .go bin
	@echo "Make folders cleaned."

$(BUILD_DIRS):
	@mkdir -p $@

$(CMD_NAMES): $(BUILD_DIRS)
	@echo "Building $(BLD_PATH_BIN)/$@$(BIN_SUFFIX)..."
	@CGO_ENABLED=0 \
	GOARCH="${ARCH}" \
	GOOS="${OS}" \
	GO111MODULE=on \
	go build \
		-installsuffix "static" \
    	-ldflags "-w -s -X main.Version=${VERSION}" \
    	-o $(BLD_PATH_GO_BIN)/$@$(BIN_SUFFIX) \
		./cmd/$@
	
	@if ! cmp -s $(BLD_PATH_GO_BIN)/$@$(BIN_SUFFIX) $(BLD_PATH_BIN)/$@$(BIN_SUFFIX); then	\
		mv $(BLD_PATH_GO_BIN)/$@$(BIN_SUFFIX) $(BLD_PATH_BIN)/$@$(BIN_SUFFIX);				\
		echo "--> $@$(BIN_SUFFIX) updated";				\
	else											\
		echo "--> $@$(BIN_SUFFIX) is already updated";	\
	fi
	@echo

build-%: ## Builds the executable files corresponding to the provided platform (OS-ARCHITECTURE)
	@$(MAKE) build                        \
	    --no-print-directory              \
	    GOOS=$(firstword $(subst _, ,$*)) \
	    GOARCH=$(lastword $(subst _, ,$*))

all-build: $(addprefix build-, $(subst /,_, $(ALL_PLATFORMS))) ## Builds the executable files for all platforms (Windows, Linux, Darwin)

build: $(BUILD_DIRS) $(CMD_NAMES) ## Builds the executable files corresponding to the current platform (OS-ARCHITECTURE)

lint: $(BUILD_DIRS) ## Lint the files
	@echo "Linting..." 
	@golint \
		-set_exit_status \
		$(PKG_LIST)
	@echo "--> Lint ran successfully"
	@echo

test: $(BUILD_DIRS) ## Run unit tests
	@echo "Running tests..." 
	@CGO_ENABLED=0 \
	GOARCH="${ARCH}" \
	GOOS="${OS}" \
	GO111MODULE=on \
	go test \
		-short \
		-installsuffix "static" \
		$(PKG_LIST)
	@echo "--> Tests ran successfully"
	@echo

test-race: $(BUILD_DIRS) ## Run data race detector
	@echo "Running race tests..." 
	@CGO_ENABLED=1 \
	GOARCH="${ARCH}" \
	GOOS="${OS}" \
	GO111MODULE=on \
	go test \
		-race \
		-short \
		-installsuffix "static" \
		$(PKG_LIST)
	@echo "--> Tests ran successfully"
	@echo

fmt: ## Detects files that need being formatted
	@echo "The following files need being gofmt'ed" 
	@find $(CODE_FOLDERS) -type f -name \*.go | xargs gofmt -l 
	@echo "--> If there were no files above this line... everything is fine"
	@echo

vet: ## Detects files that need being reviewed
	@echo "The following reports need being reviewed" 
	@go vet $(addsuffix /..., $(CODE_FOLDERS))
	@echo "--> If there were no reports above this line... everything is fine"
	@echo

scaffold: ## Creates the basic project structure folders
	@echo "Creating the basic project structure folders.." 
	@mkdir -p \
		$(CODE_FOLDERS) \
		$(CODE_PROTO_FOLDER) \
		$(CODE_ORM_FOLDER) \
		$(MIGRATIONS_FOLDER) 
	@echo "--> Folder creation completed."
	@echo

dep: ## Prepares environment for development downloading dependecies
	@echo "Downloading Project dependencies.." 
	@go mod download
	@echo "--> Dependencies download completed."
	@echo

tools: ## Installs project required tooling
	@echo "Installing Project required tools.." 
	@go get github.com/pressly/goose/cmd/goose
	@go get github.com/volatiletech/sqlboiler
	@go get github.com/glerchundi/sqlboiler-crdb
	@go get github.com/golang/mock/mockgen
	@go get github.com/lib/pq
	@echo "--> Tools download completed."
	@echo

docker-build: ## Builds the docker image
	@echo "Building Docker image..." 
	docker build -t $(CONTAINER_IMAGE_TAG):$(VERSION) -f $(CODE_DOCKERFILE) .
	@echo "--> Docker image build complete."
	@echo

docker-push: ## Pushes the docker image to the registry
	@echo "Pushing Docker image to registry..." 
	@docker push $(CONTAINER_IMAGE_TAG):$(VERSION) 
	@echo "--> Docker image push complete."
	@echo

gen-grpc: ## Generates the code corresponding to grpc services and stubs
	@echo "Generating GRPC stub code for $(CODE_PROTO_FOLDER) package..." 
	@protoc \
	--proto_path=$(CODE_PROTO_FOLDER) \
	--micro_out=$(CODE_PROTO_FOLDER) \
	--go_out=$(CODE_PROTO_FOLDER) \
	$(CODE_PROTO_FOLDER)/*.proto
	@echo "--> GRPC stubs generated."
	@echo

gen-wiring: ## Generates the wiring code to solve dependency injection references
	@echo "Generating wiring code for 'wire.go' files in packages..." 
	@find $(CODE_INTERNAL_FOLDER) -mindepth 1 -type f -name wire.go -printf '%h\n' | xargs wire
	@echo "--> Wiring code generated."
	@echo

gen-orm: ## Generates the orm code to represent the database structure
	@echo "Generating ORM code for $(CODE_ORM_FOLDER) package..." 
	@sqlboiler $(ORM_DRIVER) --output $(CODE_ORM_FOLDER) --pkgname orm --wipe --no-tests
	@echo "--> ORM code generated."
	@echo

gen-mock: ## Generates the mock code from the interfaces in domain
	@echo "Generating Mock code from $(CODE_DOMAIN_FOLDER) packages..." 
	@go generate $(CODE_DOMAIN_FOLDER)/
	@echo "--> Mock code generated."
	@echo

migrate-up: ## Applies database migration to the latest version
	@echo "Applying Upgrade migrations" 
	goose -v -dir $(MIGRATIONS_FOLDER) $(MIGRATIONS_DRIVER) "$(CONNECTION_STRING)" up 
	@echo "--> Migration applied."
	@echo

migrate-down: ## Applies database migration to the previous version
	@echo "Applying Downgrade migrations" 
	goose -v -dir $(MIGRATIONS_FOLDER) $(MIGRATIONS_DRIVER) "$(CONNECTION_STRING)" down 
	@echo "--> Migration applied."
	@echo

migrate-status: ## Obtains the database migration status
	@echo "Checking migration status" 
	goose -v -dir $(MIGRATIONS_FOLDER) $(MIGRATIONS_DRIVER) "$(CONNECTION_STRING)" status 
	@echo

help: ## Display this help screen
	@echo "Makefile for GO projects" 
	@echo
	@printf '\033[31m%s \033[0m%s \033[36m%s \033[0m\n' "NOTE:" "For this makefile to work properly, some variables are required to be set on an external file:" "variables.mk"
	@echo
	@echo "Displaying help about this makefile commands" 
	@echo
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo
	